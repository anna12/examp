package com.nc.project.service;

import java.io.IOException;

import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;

public class geoObjectService {
	
	public String getObject(String url, String type) throws JsonParseException, IOException {

		RestTemplate restTemplate = new RestTemplate();
		String result = restTemplate.getForObject(url, String.class, type);

		return result;
	}
}
