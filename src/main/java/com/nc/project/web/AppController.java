package com.nc.project.web;

import java.io.IOException;
import com.nc.project.service.geoObjectService;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonParseException;
@Controller
public class AppController {
	
	@Autowired
	private geoObjectService geoObjService;
	
	//latitude > longitude
	//amusement_park, art_gallery, cafe, food, muse, zoo, point_of_interest, establishment
	@RequestMapping(value="/{type}", produces = {"application/json; charset=UTF-8"}, method = RequestMethod.GET, params = {"lat", "lng"})
	public @ResponseBody String getObject(@PathVariable(value = "type") String type,
			@RequestParam(value = "lat") double lat, @RequestParam(value = "lng") double lng) throws JsonParseException, IOException {
		
		final String apiKey= "AIzaSyA_LK_sY9ZWjKmwDok64QZTNBP18_ojij8";  
		
		String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?" + 
					"location="+ lat + "," + lng + "&radius=1000&types={type}&language=ru&key=" + apiKey;
				
		return geoObjService.getObject(url, type);		

	}

	
	@RequestMapping(value="/login")
	public String logInUser() {
			
		return "";
	}

	@RequestMapping("/logon")
	public String logOnUser() {
		return null;
	}
	
}
