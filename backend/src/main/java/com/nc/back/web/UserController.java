package com.nc.back.web;
 
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
 
/**
 * ���������� ��� ������� �������� ����������.
 */
@Controller
public class UserController {
 
 private int visitorCount = 0;
 
 @RequestMapping("/")
 public String index(Model model) {
    model.addAttribute("visitorCount", visitorCount++);
    return "index";
 }
 
}