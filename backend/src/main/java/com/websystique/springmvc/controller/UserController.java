package com.websystique.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/user")
public class UserController {
	
	@RequestMapping(value="/account", method = RequestMethod.GET)
	public String getUserAccount() {
		return null;
	}

	@RequestMapping(value="/liked", method = RequestMethod.GET)
	public String getLikedObjects() {
		return null;
	}
	
	@RequestMapping(value="/routes", method = RequestMethod.GET)
	public String getRoutes() {
		return null;
	}
	
}
