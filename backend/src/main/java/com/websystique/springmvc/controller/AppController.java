package com.websystique.springmvc.controller;

import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@Controller
public class AppController {

	//latitude > longitude
	@RequestMapping(value="/museum", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET, params={"lat", "lng"})
	public JSONObject getMuseum(@RequestParam(value = "lat") double lat, 
			@RequestParam(value = "lng") double lng) throws IOException {
		
		final String apiKey= "e2dd823b-07bb-45cc-81fe-83271b180636"; 

		String url = "https://search-maps.yandex.ru/v1/?text=�����&"
				+ "ll="+ lng + "," + lat +"&sspn=0.035&lang=ru_RU&"
				+ "apikey=" + apiKey;
		
		RestTemplate restTemplate = new RestTemplate();
		JSONObject result = restTemplate.getForObject(url, JSONObject.class);
				
		return result;
	}

	@RequestMapping(value="/login")
	public String logInUser() {
			
		return "";
	}

	@RequestMapping("/logon")
	public String logOnUser() {
		return null;
	}
	
}
